## == download talos metal image
resource "null_resource" "get_talos_tgz" {
  provisioner "local-exec" {
    command = "export talos_version=${var.talos_version} ; /bin/bash helpers/get-images.sh"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "/bin/bash helpers/del-images.sh"
  }
}

## == create talos config files
resource "null_resource" "get_talos_config" {
  depends_on  = [libvirt_domain.domain-talos-controlplane-raw, libvirt_domain.domain-talos-worker-raw]
  provisioner "local-exec" {
    command = <<EOT
              export cluster_name=${var.cluster_name}
              export k8s_network=${var.k8s_network0}
              export controlplane_name=${var.controlplane_name}
              export worker_name=${var.worker_name}
              export controlplane_instances=${var.controlplane_instances}
              export worker_instances=${var.worker_instances}
              export talos_version=${var.talos_version}
              /bin/bash helpers/gen-config.sh
              EOT
  }
  provisioner "local-exec" {
    when    = destroy
    command = "/bin/bash helpers/del-config.sh"
  }
}
