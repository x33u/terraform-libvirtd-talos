## == talos cluster name
variable "cluster_name" {
  default = "local-talos-cluster"
}
## == talos image version
variable "talos_version" {
  type    = string
  default = "v1.9.1"
}


## == controlplane node configuration
variable "controlplane_name" {
  default = "talos-controlplane"
}
variable "controlplane_instances" {
  default = "3"
}
variable "controlplane-memory" {
  default = "2048"
}
variable "controlplane-vcpu" {
  default = "2"
}
variable "controlplane-diskBytes" {
  default = 1024*1024*1024*10 #10GB
}
variable "extra-storage-controlplane-diskBytes" {
  default = 1024*1024*1024*10 #10GB
}


## == worker node configuration
variable "worker_name" {
  default = "talos-worker"
}
variable "worker_instances" {
  default = "3"
}
variable "worker-memory" {
  default = "1024"
}
variable "worker-vcpu" {
  default = "1"
}
variable "worker-diskBytes" {
  default = 1024*1024*1024*10 #10GB
}
variable "extra-storage-worker-diskBytes" {
  default = 1024*1024*1024*10 #10GB
}


## == primary network variables
variable "network-name0"{
  default = "k8snet-0"
}
variable "domain-name0"{
  default = "k8s-0.local"
}
variable "k8s_network0"{
  default = "10.17.3." #first three octets are customizable
}

## == additional network variables
variable "network-name1"{
  default = "k8snet-private"
}
variable "domain-name1"{
  default = "k8s-1.local"
}
variable "k8s_network1"{
  default = "10.17.4." #first three octets are customizable
}

