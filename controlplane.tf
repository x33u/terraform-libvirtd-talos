## == controlplane node domain config
resource "libvirt_domain" "domain-talos-controlplane-raw" {
  count       = var.controlplane_instances
  depends_on  = [libvirt_network.kube_network0]
  name        = "${var.controlplane_name}-${count.index}"
  memory      = var.controlplane-memory
  vcpu        = var.controlplane-vcpu
  autostart   = "true"
  machine     = "q35"
  boot_device {
    dev = [ "hd" ]
  }
  disk {
    volume_id = libvirt_volume.vol-talos-controlplane-raw[count.index].id
  }
  disk {
    volume_id = libvirt_volume.extra-vol-talos-controlplane-qcow[count.index].id
  }
  cpu {
    mode = "host-passthrough"
  }
  network_interface {
    network_name   = "${var.network-name0}"
    hostname	      = "${var.controlplane_name}-${count.index}"
    addresses	     = ["${var.k8s_network0}20${count.index}"]
    mac            = "AA:BB:CC:11:20:0${count.index}"
    wait_for_lease = "true"
  }
  network_interface {
    network_name   = "${var.network-name1}"
    hostname	      = "${var.controlplane_name}-${count.index}"
    addresses	     = ["${var.k8s_network1}20${count.index}"]
    mac            = "AA:BB:CC:11:30:0${count.index}"
    wait_for_lease = "true"
  }
  graphics {
    type        = "vnc"
    listen_type = "none"
  }
}
