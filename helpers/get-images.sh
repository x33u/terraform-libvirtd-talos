#!/bin/sh

## create image dir
if [ ! -d images/$talos_version ]; then
  mkdir -p images/$talos_version
fi


## == get talosctl and make it executable
if [ -f images/$talos_version/talosctl-linux-amd64 ]; then
    echo '... "talosctl-linux-amd64" already exists'
else
    echo -e '\e[0;32m downloading "talosctl-linux-amd64"\e[0m' ;
    curl https://github.com/siderolabs/talos/releases/download/$talos_version/talosctl-linux-amd64 \
      -L -o images/$talos_version/talosctl-linux-amd64 ; chmod +x images/$talos_version/talosctl-linux-amd64
fi

## == get metal image
talos_version_xz="v1.4.8"
talos_version_zst="v1.8.0"
if [ "$(printf '%s\n' "$talos_version_zst" "$talos_version" | sort -V | head -n1)" = "$talos_version_zst" ]; then
    if [ -f images/$talos_version/metal-amd64.raw.zst ]; then
        echo '... "metal-amd64.raw.zst" image already exists'
    else
        echo -e '\e[0;32mdownloading "metal-amd64.raw.zst"\e[0m' ;
        curl https://github.com/siderolabs/talos/releases/download/$talos_version/metal-amd64.raw.zst \
        -L -o images/$talos_version/metal-amd64.raw.zst
    fi
elif [ "$(printf '%s\n' "$talos_version_xz" "$talos_version" | sort -V | head -n1)" = "$talos_version_xz" ]; then
    if [ -f images/$talos_version/metal-amd64.raw.xz ]; then
        echo '... "metal-amd64.raw.xz" image already exists'
    else
        echo -e '\e[0;32mdownloading "metal-amd64.raw.xz"\e[0m' ;
        curl https://github.com/siderolabs/talos/releases/download/$talos_version/metal-amd64.raw.xz \
        -L -o images/$talos_version/metal-amd64.raw.xz
    fi
else
    if [ -f images/$talos_version/metal-amd64.tar.gz ]; then
        echo '... "metal-amd64.tar.gz" image already exists'
    else
        echo -e '\e[0;32mdownloading "metal-amd64.tar.gz"\e[0m' ;
        curl https://github.com/siderolabs/talos/releases/download/$talos_version/metal-amd64.tar.gz \
        -L -o images/$talos_version/metal-amd64.tar.gz
    fi
fi


## == extract image
if [ -f images/$talos_version/disk.raw ]; then
    echo '... "disk.raw" already exists'
else
    ## extract .zst
    if [ "$(printf '%s\n' "$talos_version_zst" "$talos_version" | sort -V | head -n1)" = "$talos_version_zst" ]; then
        echo "greater than or equal to talos ${talos_version_zst}"
        unzstd images/$talos_version/metal-amd64.raw.zst && \
            mv images/$talos_version/metal-amd64.raw images/$talos_version/disk.raw
    ## extract .xz
    elif [ "$(printf '%s\n' "$talos_version_xz" "$talos_version" | sort -V | head -n1)" = "$talos_version_xz" ]; then
        echo "greater than or equal to talos ${talos_version_xz}"
        unxz images/$talos_version/metal-amd64.raw.xz && \
            mv images/$talos_version/metal-amd64.raw images/$talos_version/disk.raw
    ## extract .gz
    else
        tar xzf images/$talos_version/metal-amd64.tar.gz -C images/$talos_version/
    fi
fi

