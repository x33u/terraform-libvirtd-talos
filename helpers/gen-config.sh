#!/bin/bash


## create talos config dir
if [ ! -d talos ]; then
  mkdir talos
fi


## == generate talos secrets file
if [ -f talos/secrets.yaml ]; then
    echo '... "secrets.yaml" already exist'
else
    ./images/"$talos_version"/talosctl-linux-amd64 gen secrets \
    --output-file talos/secrets.yaml
fi


## == generate talos config files
if [ -f talos/talosconfig ]; then
    echo '... "talosconfig" already exist'
else
    ./images/"$talos_version"/talosctl-linux-amd64 gen config \
    "$cluster_name" \
    https://"$k8s_network"200:6443 \
    --with-secrets talos/secrets.yaml \
    --output-dir talos/ \
    --config-patch-control-plane @helpers/talos-controlplane-patch.yaml \
    --config-patch-worker @helpers/talos-worker-patch.yaml \
    && sleep 2 \
    && sed -i 's\/dev/sda\/dev/vda\g' \
       talos/controlplane.yaml \
       talos/worker.yaml \
    && ./images/"$talos_version"/talosctl-linux-amd64 validate \
       --mode metal \
       --config talos/controlplane.yaml \
    && ./images/"$talos_version"/talosctl-linux-amd64 validate \
       --mode metal \
       --config talos/worker.yaml
fi


## == apply talos config files on all nodes
if [ -f talos/kubeconfig ]; then
    echo '... "kubeconfig" already exists - please apply config manually'
else
   sleep 2 \
    && for (( i=0; i<"$controlplane_instances"; i++ ))
       do
         echo "send config to node '$controlplane_name-$i'" ; \
         sleep 2 ; ./images/"$talos_version"/talosctl-linux-amd64 apply-config --insecure --nodes "$k8s_network"20"$i" --file talos/controlplane.yaml
       done
       for (( i=0; i<"$worker_instances"; i++ ))
       do
         echo "send config to node '$worker_name-$i'" ; \
         sleep 2 ; ./images/"$talos_version"/talosctl-linux-amd64 apply-config --insecure --nodes "$k8s_network"22"$i" --file talos/worker.yaml
       done
       sleep 2 \
       && ./images/"$talos_version"/talosctl-linux-amd64 --talosconfig talos/talosconfig \
          config endpoint "$k8s_network"200 \
       && sleep 2 \
       && ./images/"$talos_version"/talosctl-linux-amd64 --talosconfig talos/talosconfig \
          config node "$k8s_network"200 \
       && echo -e 'apply node config may take a while ...' \
       && until ./images/"$talos_version"/talosctl-linux-amd64 --talosconfig talos/talosconfig bootstrap &> /dev/null
          do
            echo "waiting for nodes become ready to bootstrapping ..."
            sleep 2
          done
       echo "bootstrap command send!"
fi



## == create kubeconfig
if [ -f talos/kubeconfig ]; then
    echo 'kubeconfig already in place - bootstrap manually'
else
  ./images/"$talos_version"/talosctl-linux-amd64 --talosconfig talos/talosconfig kubeconfig talos/kubeconfig
fi


## == echo'ing final instructions
echo -e 'finishing bootstrap can take a few minutes' \
 && echo -e 'when finished run:\e[0;32m "kubectl --kubeconfig talos/kubeconfig get nodes" \e[0m' \
 && echo    '---------------------------------------------------------------------'

