### talos linux on libvirtd

a terraform or [opentofu](https://opentofu.org/) script that spawns [talos linux](https://www.talos.dev/) into your libvirt installation

prerequisite packages:

- `terraform` or `opentofu`
- `curl`
- `tar`
- `xz`

to create resources run:

```shell
# initilalize resources
> tofu init

# apply resources
> tofu apply
```

a few minutes after the script is done do test the connection

```shell
# test connection
> kubectl --kubeconfig talos/kubeconfig get nodes
```

to destroy resources run:

```shell
# destroy resources
> tofu destroy
```

for details about using talos see: [talos-guides](https://www.talos.dev/v1.2/talos-guides/)

<br>

---

### docs

* [add additional options](https://codeberg.org/x33u/terraform-libvirtd-talos#add-additional-options)
* [pre existent secrets](https://codeberg.org/x33u/terraform-libvirtd-talos#pre-existent-secrets)
* [patch talosconfig](https://codeberg.org/x33u/terraform-libvirtd-talos#patch-talosconfig)
* [scaling nodes](https://codeberg.org/x33u/terraform-libvirtd-talos#scaling-nodes)
* [add an external loadbalancer](https://codeberg.org/x33u/terraform-libvirtd-talos#add-an-external-loadbalancer)

#### add additional options

since the only thing that happens is a shell script on top terraform,
we can pass additional options by adding these behind the _$cluster_name_ variable in the `variables.tf` file

eg. for passing `--with-kubespan`

```yaml
## == talos cluster name
variable "cluster_name" {
  default = "local-talos-cluster --with-kubespan"
}
```

#### pre existent secrets

the auto created `secrets.yaml` file can also be pre existent:

```shell
# create config folder
> mkdir talos

# generate talos secrets
> talosctl gen secrets \
    --output-file talos/secrets.yaml
```

#### patch talosconfig

to patch machine config edit the `.yaml` patch files in `helpers/` directory

```less
helpers
├── del-config.sh
├── del-images.sh
├── gen-config.sh
├── get-images.sh
├── talos-controlplane-patch.yaml << controlplane
└── talos-worker-patch.yaml       << worker
```

#### scaling nodes:

to scale nodes you have to modify `variables.tf`
<br>
edit the instances variable eg. `worker_instances` to 4 nodes.
```shell
## == worker node configuration
...
variable "worker_instances" {
  default = "4"
}

```

apply variable changes by using terraform
```shell
# run terraform
> terraform apply
```

> !note: when scale **up** you need to `apply-config` by using `talosctl` on that node(s)

<br>

to apply Talos config on the scaled node(s) find out the IP for the new host `talos-worker-3`
```shell
# use virsh to get dhcp leases
> virsh net-dhcp-leases --network k8snet-0

 Expiry Time           MAC address         Protocol   IP address       Hostname               Client ID or DUID
-----------------------------------------------------------------------------------------------------------------
 2022-10-20 14:48:12   aa:bb:cc:11:20:00   ipv4       10.17.3.200/24   talos-controlplane-0   -
 2022-10-20 14:48:12   aa:bb:cc:11:20:01   ipv4       10.17.3.201/24   talos-controlplane-1   -
 2022-10-20 14:48:12   aa:bb:cc:11:20:02   ipv4       10.17.3.202/24   talos-controlplane-2   -
 2022-10-20 14:48:12   aa:bb:cc:11:20:20   ipv4       10.17.3.220/24   talos-worker-0         -
 2022-10-20 14:48:11   aa:bb:cc:11:20:21   ipv4       10.17.3.221/24   talos-worker-1         -
 2022-10-20 14:48:12   aa:bb:cc:11:20:22   ipv4       10.17.3.222/24   talos-worker-2         -
 2022-10-20 14:36:17   aa:bb:cc:11:20:23   ipv4       10.17.3.223/24   talos-worker-3         -
```

so, the target Ip is `10.17.3.223`.<br>
lets do `apply-config` on that node.<br>
since this node is a worker we choose the `worker.yaml`
```shell
# apply talos config
talosctl apply-config \
  --insecure --nodes "10.17.3.223" \
  --file talos/worker.yaml
```

when apply-config is done we can see the new node
```shell
NAME                   STATUS   ROLES           AGE     VERSION
talos-controlplane-0   Ready    control-plane   3h56m   v1.25.2
talos-controlplane-1   Ready    control-plane   3h56m   v1.25.2
talos-controlplane-2   Ready    control-plane   3h55m   v1.25.2
talos-worker-0         Ready    <none>          3h55m   v1.25.2
talos-worker-1         Ready    <none>          3h56m   v1.25.2
talos-worker-2         Ready    <none>          3h56m   v1.25.2
talos-worker-3         Ready    <none>          45s     v1.25.2
```

#### add an external loadbalancer

to add an external loadbalancer you can follow the instructions on: 
[codeberg.org/x33u/almalinux-terraform-cloudinit](https://codeberg.org/x33u/almalinux-terraform-cloudinit#almalinux-terraform-cloudinit)
