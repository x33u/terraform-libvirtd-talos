## == create virsh primary network
resource "libvirt_network" "kube_network0" {
  name      = "${var.network-name0}"
  domain    = "${var.domain-name0}"
  mode      = "nat"
  addresses = ["${var.k8s_network0}0/24"]
  autostart = "true"
  dhcp {
    enabled = "true"
  }
  dns {
    enabled = "true"
    local_only = "false"
  }
}

## == create virsh additional network
resource "libvirt_network" "kube_network1" {
  name      = "${var.network-name1}"
  domain    = "${var.domain-name1}"
  mode      = "nat"
  addresses = ["${var.k8s_network1}0/24"]
  autostart = "true"
  dhcp {
    enabled = "true"
  }
  dns {
    enabled = "true"
    local_only = "false"
  }
}

