## == worker node domain config
resource "libvirt_domain" "domain-talos-worker-raw" {
  count       = var.worker_instances
  depends_on  = [libvirt_network.kube_network0]
  name        = "${var.worker_name}-${count.index}"
  memory      = var.worker-memory
  vcpu        = var.worker-vcpu
  autostart   = "true"
  machine     = "q35"
  boot_device {
    dev = [ "hd" ]
  }
  disk {
    volume_id = libvirt_volume.vol-talos-worker-raw[count.index].id
  }
  disk {
    volume_id = libvirt_volume.extra-vol-talos-worker-qcow[count.index].id
  }
  cpu {
    mode = "host-passthrough"
  }
  network_interface {
    network_name   = "${var.network-name0}"
    hostname   	   = "${var.worker_name}-${count.index}"
    addresses	     = ["${var.k8s_network0}22${count.index}"]
    mac            = "AA:BB:CC:11:20:2${count.index}"
    wait_for_lease = "true"
  }
  network_interface {
    network_name   = "${var.network-name1}"
    hostname   	   = "${var.worker_name}-${count.index}"
    addresses	     = ["${var.k8s_network1}22${count.index}"]
    mac            = "AA:BB:CC:11:30:2${count.index}"
    wait_for_lease = "true"
  }
  graphics {
    type        = "vnc"
    listen_type = "none"
  }
}
