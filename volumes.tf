## == create talos raw baseimage
resource "libvirt_volume" "talos-raw" {
  depends_on = [null_resource.get_talos_tgz]
  name   = "talos-raw"
  format = "raw"
  source = "images/${var.talos_version}/disk.raw"
}

## == create controlplane node volumes from baseimage
resource "libvirt_volume" "vol-talos-controlplane-raw" {
  count       = var.controlplane_instances
  name           = "vol-talos-controlplane-raw-${count.index}"
  base_volume_id = libvirt_volume.talos-raw.id
  size = var.controlplane-diskBytes
}

## == create worker node volumes from baseimage
resource "libvirt_volume" "vol-talos-worker-raw" {
  count       = var.worker_instances
  name           = "vol-talos-worker-raw-${count.index}"
  base_volume_id = libvirt_volume.talos-raw.id
  size = var.worker-diskBytes
}

## == create extra volumes for controlplane nodes
resource "libvirt_volume" "extra-vol-talos-controlplane-qcow" {
  count       = var.controlplane_instances
  name           = "extra-vol-talos-controlplane-qcow-${count.index}"
  size = var.extra-storage-controlplane-diskBytes
}

## == create extra volumes for worker nodes
resource "libvirt_volume" "extra-vol-talos-worker-qcow" {
  count       = var.worker_instances
  name           = "extra-vol-talos-worker-qcow-${count.index}"
  size = var.extra-storage-worker-diskBytes
}
