### rook-ceph example

a rook-ceph helm chart example including value files

> recommend cluster utilisation in `variables.tf` would `3cpu`, `4gb ram` per node

```shell
# add repo
> helm repo add rook-release https://charts.rook.io/release

# run operator
> helm install --create-namespace --namespace rook-ceph \
  rook-ceph rook-release/rook-ceph \
  -f operator-values.yaml \
  --version 1.9.7

# WARN: allowPrivilegeEscalation
> kubectl label ns rook-ceph \
  pod-security.kubernetes.io/enforce=privileged

# run cluster when operator is running
> helm install --namespace rook-ceph \
  rook-ceph-cluster rook-release/rook-ceph-cluster \
  -f cluster-values.yaml \
  --version 1.9.7
```
