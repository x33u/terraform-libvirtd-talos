### Talos Linux patching example

Patches can be applied by generating talos config or even after bootstrapping


#### Generate config including patch

Apply patch on all type of nodes
```shell
> talosctl gen config \
  local-talos-cluster \
  https://10.17.3.200:6443 \
  --output-dir talos/ \
  --config-patch @patch.yaml
```

Apply different patches on all type of nodes
```shell
> talosctl gen config \
  local-talos-cluster \
  https://10.17.3.200:6443 \
  --output-dir talos/ \
  --config-patch-control-plane @controlplane-patch.yaml \
  --config-patch-worker @worker-patch.yaml
```

Patch running cluster
```shell
# apply patch
> talosctl -e 10.17.3.200 -n 10.17.3.202 \
  patch mc -p @patch.yaml
```

To install the system extension, the node needs to be upgraded.
The node can be upgraded to the same version as the existing Talos version.
```shell
> talosctl -e 10.17.3.200 -n 10.17.3.202 \
  upgrade --image=ghcr.io/siderolabs/installer:v1.1.2
```

Check if extension is installed
```shell
> talosctl -n 10.17.3.202 \
  get extensions
```
